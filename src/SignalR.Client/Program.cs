﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using SignalR.Model;

var connection = new HubConnectionBuilder()
    .WithUrl("https://localhost:5001/hubs/customer")
    .Build();

var actions = new Dictionary<char, string>()
{
    { 'L', "[L]ist of customers" },
    { 'G', "[G]et customer" },
    { 'A', "[A]dd customer" },
    { 'U', "[U]pdate customer" },
    { 'D', "[D]elete customer" },
    { 'E', "[E]xit" }
};

void DisplayActionList()
{
    Console.WriteLine("Choose action:");
    Console.WriteLine("");

    var foregroundColor = Console.ForegroundColor;

    foreach (var action in actions)
    {
        var index1 = action.Value.IndexOf('[');
        var index2 = action.Value.IndexOf(']');
        var substr1 = action.Value.Substring(index1, 1);
        var substr2 = action.Value.Substring(index1 + 1, 1);
        var substr3 = action.Value.Substring(index2, action.Value.Length - index2);

        Console.ForegroundColor = foregroundColor;
        Console.Write(substr1);

        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.Write(substr2);

        Console.ForegroundColor = foregroundColor;
        Console.Write(substr3);
        Console.WriteLine();
    }

    Console.WriteLine("");
}

string Delimeter = new('=', 50);

void DisplayResult(List<string> result)
{
    Console.WriteLine(Delimeter);
    Console.WriteLine("");

    var foregroundColor = Console.ForegroundColor;

    foreach (var item in result)
    {
        var pos = item.IndexOf(':');

        if (pos < 0)
        {
            Console.ForegroundColor = foregroundColor;
            Console.Write(item);
        }
        else
        {
            var substr1 = item.Substring(0, pos + 1);
            var substr2 = item.Substring(pos + 1, item.Length - pos - 1);

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(substr1);
            Console.ForegroundColor = foregroundColor;
            Console.Write(substr2);
        }

        Console.WriteLine();
    }

    Console.WriteLine(Delimeter);
    Console.WriteLine("");
}

Dictionary<string, string> GetParameters(char inputAction)
{
    return inputAction switch
    {
        'L' => new Dictionary<string, string>(),
        'G' => new Dictionary<string, string>()
                {
                    { "Id", "Id" }
                },
        'A' => new Dictionary<string, string>()
                {
                    { "FirstName", "FirstName" },
                    { "LastName", "LastName" },
                    { "Email", "Email" },
                    { "PreferenceIds", "List of preferences with ; delimeter" }
                },
        'U' => new Dictionary<string, string>()
                {
                    { "Id", "Id" },
                    { "FirstName", "FirstName" },
                    { "LastName", "LastName" },
                    { "Email", "Email" },
                    { "PreferenceIds", "List of preferences with ; delimeter" }
                },
        'D' => new Dictionary<string, string>()
                {
                    { "Id", "Id" }
                },
        _ => new Dictionary<string, string>()
    };
}

List<ActionParameter> GetParameterValues()
{
    var inputAction = char.ToUpper(Console.ReadLine()?[0] ?? default);

    if (!actions.ContainsKey(inputAction))
    {
        Console.WriteLine("Incorrect action !");
        Console.WriteLine("");
        DisplayActionList();
    }

    var parameters = GetParameters(inputAction);

    var actionParameters = new List<ActionParameter>();

    foreach (var parameter in parameters)
    {
        Console.WriteLine($"Enter '{parameter.Value}': ");

        var parameterValue = Console.ReadLine();

        var actionParameter = new ActionParameter
        {
            Action = inputAction,
            ParameterName = parameter.Key,
            ParameterValue = parameterValue
        };

        actionParameters.Add(actionParameter);
    }

    if (actionParameters.Count == 0)
    {
        var actionParameter = new ActionParameter
        {
            Action = inputAction,
            ParameterName = null,
            ParameterValue = null
        };

        actionParameters.Add(actionParameter);
    }

    return actionParameters;
}

#region Query processing

bool continueInput = true;

while (continueInput)
{
    DisplayActionList();
    
    var parameters = GetParameterValues();

    var action = parameters.Select(p => p.Action).First();

    switch (action)
    {
        case 'L':
            await GetCustomers();
            break;
        case 'G':
            await GetCustomer(parameters);
            break;
        case 'A':
            await CreateCustomer(parameters);
            break;
        case 'U':
            await UpdateCustomer(parameters);
            break;
        case 'D':
            await DeleteCustomer(parameters);
            break;
        case 'E':
            continueInput = false;
            break;
        default:
            continueInput = false;
            break;
    }
}

#endregion

async Task GetCustomers()
{
    await connection.StartAsync();

    var result = new List<string>();

    connection.On("GetCustomersOnClient", (List<CustomerShortResponse> customers) =>
    {
        foreach (var customer in customers)
        {
            result.Add($"Id: {customer.Id}");
            result.Add($"FirstName: {customer.FirstName}");
            result.Add($"LastName: {customer.LastName}");
            result.Add($"Email: {customer.Email}");
            result.Add("");
        }
    });

    await connection.InvokeAsync("GetCustomers");
    await connection.StopAsync();

    DisplayResult(result);
}

async Task GetCustomer(List<ActionParameter> parameters)
{
    var result = new List<string>();

    var check = Guid.TryParse(parameters.First().ParameterValue, out var id);

    if (!check)
    {
        DisplayResult(result);
        return;
    }

    await connection.StartAsync();

    connection.On("GetCustomerOnClient", (CustomerResponse customer) =>
    {
        result.AddRange(GetCustomerResponse(customer));
    });

    await connection.InvokeAsync("GetCustomer", id);
    await connection.StopAsync();

    DisplayResult(result);
}

async Task CreateCustomer(List<ActionParameter> parameters)
{
    var paramRequest = PrepareCreateOrEditCustomerRequest(parameters);

    await connection.StartAsync();

    var result = new List<string>();

    connection.On("CreateCustomerOnClient", (CustomerResponse customer) =>
    {
        result.AddRange(GetCustomerResponse(customer));
    });

    await connection.InvokeAsync("CreateCustomer", paramRequest);
    await connection.StopAsync();

    DisplayResult(result);
}

async Task UpdateCustomer(List<ActionParameter> parameters)
{
    var result = new List<string>();

    var check = Guid.TryParse(parameters.Where(p => p.ParameterName == "Id").First().ParameterValue, out var paramId);

    if (!check)
    {
        DisplayResult(result);
        return;
    }

    var paramRequest = PrepareCreateOrEditCustomerRequest(parameters);

    await connection.StartAsync();

    connection.On("UpdateCustomerOnClient", (CustomerResponse customer) =>
    {
        result.AddRange(GetCustomerResponse(customer));
    });

    await connection.InvokeAsync("UpdateCustomer", paramId, paramRequest);
    await connection.StopAsync();

    DisplayResult(result);
}

async Task DeleteCustomer(List<ActionParameter> parameters)
{
    var result = new List<string>();

    var check = Guid.TryParse(parameters.First().ParameterValue, out var id);

    if (!check)
    {
        DisplayResult(result);
        return;
    }

    await connection.StartAsync();

    connection.On("DeleteCustomerOnClient", () =>
    {
        result.Add("Customer was deleted");
        result.Add("");
    });

    await connection.InvokeAsync("DeleteCustomer", id);
    await connection.StopAsync();

    DisplayResult(result);
}

List<string> GetCustomerResponse(CustomerResponse customer)
{
    var result = new List<string>
    {
        $"Id: {customer.Id}",
        $"FirstName: {customer.FirstName}",
        $"LastName: {customer.LastName}",
        $"Email: {customer.Email}"
    };

    if ((customer.Preferences?.Count ?? 0) > 0)
    {
        var prefNames = customer.Preferences.Select(p => p.Name).ToArray();
        var preferences = string.Join(", ", prefNames);
        result.Add($"Preferences: {preferences}");
    }

    result.Add("");

    return result;
}

CreateOrEditCustomerRequest PrepareCreateOrEditCustomerRequest(List<ActionParameter> parameters)
{
    var firstName = parameters.Where(p => p.ParameterName == "FirstName").First().ParameterValue;
    var lastName = parameters.Where(p => p.ParameterName == "LastName").First().ParameterValue;
    var email = parameters.Where(p => p.ParameterName == "Email").First().ParameterValue;

    var preferenceIds = parameters.Where(p => p.ParameterName == "PreferenceIds").First().ParameterValue;

    var prefGuids = preferenceIds.Split(';')
        .Select(p => Guid.TryParse(p, out var g) ? g : (Guid?)null)
        .Where(p => p != null)
        .Select(p => p.Value)
        .ToList();

    return new CreateOrEditCustomerRequest
    {
        FirstName = firstName,
        LastName = lastName,
        Email = email,
        PreferenceIds = prefGuids
    };
}

public class ActionParameter
{
    public char Action { get; set; }
    public string ParameterName { get; set; }
    public string ParameterValue { get; set; }
}
﻿using System;

namespace SignalR.Model
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
using System;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace GrpcServer.Services
{
    public class CustomerService : CustomerGrpc.CustomerGrpcBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerService(
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository
            )
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async override Task<CustomerShortResponseList> GetCustomers(EmptyRequest request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            var result = new CustomerShortResponseList();
            result.Customers.AddRange(response);

            return result;
        }

        public async override Task<CustomerResponse> GetCustomer(CustomerIdRequest request, ServerCallContext context)
        {
            var customer = await GetCustomer(request.Id);

            var response = new CustomerResponse()
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };

            response.Preferences.AddRange(
                customer.Preferences.Select(x => new PreferenceResponse()
                {
                    Id = x.PreferenceId.ToString(),
                    Name = x.Preference.Name
                }));

            return response;
        }

        public async override Task<CustomerResponse> CreateCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            var preferencesGuids = request.PreferenceIds.Select(Guid.Parse).ToList();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferencesGuids);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            var idRequest = new CustomerIdRequest() { Id = customer.Id.ToString() };
            var result = await GetCustomer(idRequest, context);

            return result;
        }

        public async override Task<CustomerResponse> EditCustomer(EditCustomerRequest request, ServerCallContext context)
        {
            var customer = await GetCustomer(request.Id);

            var preferencesGuids = request.EditRequest.PreferenceIds.Select(Guid.Parse).ToList();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferencesGuids);

            CustomerMapper.MapFromModel(request.EditRequest, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            var idRequest = new CustomerIdRequest() { Id = customer.Id.ToString() };
            var result = await GetCustomer(idRequest, context);

            return result;
        }

        public async override Task<EmptyResponse> DeleteCustomer(CustomerIdRequest request, ServerCallContext context)
        {
            var customer = await GetCustomer(request.Id);

            await _customerRepository.DeleteAsync(customer);

            return null;
        }

        private async Task<Customer> GetCustomer(string id)
        {
            var guidId = Guid.Parse(id);
            var customer = await _customerRepository.GetByIdAsync(guidId);

            if (customer == null)
            {
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found"));
            }

            return customer;
        }
    }
}
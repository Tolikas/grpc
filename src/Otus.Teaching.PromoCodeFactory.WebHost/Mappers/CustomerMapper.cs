﻿﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class CustomerMapper
    {
        public static Customer MapFromModel(GrpcServer.CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer = null)
        {
            customer ??= new Customer()
            {
                Id = Guid.NewGuid()
            };

            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            if (customer.Preferences?.Count() > 0)
            {
                customer.Preferences.Clear();
            }

            if (preferences?.Count() > 0)
            {
                customer.Preferences = preferences.Select(x => new CustomerPreference()
                {
                    CustomerId = customer.Id,
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList();
            }

            return customer;
        }
    }
}
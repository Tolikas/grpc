using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using SignalR.Model;
using System.Collections.Generic;
using SignalR.Server.Mappers;

namespace SignalR.Server
{
    public class CustomerHub : Hub<ICustomerHubClient>
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IHubContext<CustomerHub, ICustomerHubClient> _hubContext;

        public CustomerHub(
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository,
            IHubContext<CustomerHub, ICustomerHubClient> hubContext)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _hubContext = hubContext;
        }

        public async Task GetCustomers()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            await _hubContext.Clients.All.GetCustomersOnClient(response);
        }
        
        public async Task GetCustomer(Guid id)
        {
            var response = await GetCustomerResponse(id);
            await _hubContext.Clients.All.GetCustomerOnClient(response);
        }

        public async Task CreateCustomer(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            var customer = CustomerMapper.MapFromModel(request, preferences);
            await _customerRepository.AddAsync(customer);
            var response = await GetCustomerResponse(customer.Id);
            await _hubContext.Clients.All.CreateCustomerOnClient(response);
        }

        public async Task UpdateCustomer(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await GetCustomerById(id);

            if (customer.Id != Guid.Empty)
            {
                var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
                CustomerMapper.MapFromModel(request, preferences, customer);
                await _customerRepository.UpdateAsync(customer);
            }

            var response = GetCustomerResponse(customer);
            await _hubContext.Clients.All.UpdateCustomerOnClient(response);
        }

        public async Task DeleteCustomer(Guid id)
        {
            var customer = await GetCustomerById(id);

            if (customer.Id != Guid.Empty)
            {
                await _customerRepository.DeleteAsync(customer);
            }

            await _hubContext.Clients.All.DeleteCustomerOnClient();
        }

        private async Task<Customer> GetCustomerById(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                const string NotFound = "Empty";

                customer = new Customer()
                {
                    Id = Guid.Empty,
                    FirstName = NotFound,
                    LastName = NotFound,
                    Email = NotFound,
                    Preferences = new List<CustomerPreference>()
                };
            }

            return customer;
        }

        private async Task<CustomerResponse> GetCustomerResponse(Guid id)
        {
            var customer = await GetCustomerById(id);
            return GetCustomerResponse(customer);
        }

        private static CustomerResponse GetCustomerResponse(Customer customer)
        {
            var preferences = customer.Preferences?
                .Select(x => new PreferenceResponse()
                {
                    Id = x.PreferenceId,
                    Name = x.Preference.Name
                })
                .ToList();

            var response = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences = preferences
            };

            return response;
        }
    }

    public interface ICustomerHubClient
    {
        Task GetCustomersOnClient(List<CustomerShortResponse> customers);
        Task GetCustomerOnClient(CustomerResponse customer);
        Task CreateCustomerOnClient(CustomerResponse customer);
        Task UpdateCustomerOnClient(CustomerResponse customer);
        Task DeleteCustomerOnClient();
    }
}